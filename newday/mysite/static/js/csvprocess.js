var current_page1 = 1;
var current_page2 = 2;
var current_page3 = 3;
    
function CsvSubmit(){
   
   var reader = new FileReader();
   reader.readAsText(document.getElementById("myFile").files[0], 'utf-8');
   reader.onload = function (e) {
        
        csvdata = e.target.result;
        
        var crf_token = "";
            document.cookie.split("; ").forEach(function(obj, index) {
            
                var cookie = obj.split('=');
                if(cookie[0] == 'csrftoken') {
                    crf_token = cookie[1];
                }
            })
           
            $.ajax({
                type: "POST",
                url: "/upload/",
                data: {'csvdata': csvdata
                },
              
                headers: {'X-CSRFToken': crf_token },
                success: function (response) {
                    alert('success');
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    };
                    console.log(msg)
                }
            });
    }
   
    return false;
}


function NaturalTour(){    
    localStorage.removeItem("tourdata");
    var tour_item = "one" + ";" + "Natural History";
    TourFind(tour_item)
    return false;
}

function LandmarkTour(){ 
    localStorage.removeItem("tourdata");  
    var tour_item = "one" + ";" + "Landmark";
    TourFind(tour_item)
    return false;
}

function MuseumTour(){ 
    localStorage.removeItem("tourdata");  
    var tour_item = "one" + ";" + "Science Museum";
    TourFind(tour_item)
    return false;
}

function ThreeTour(){ 
    localStorage.removeItem("tourdata");  
    var tour_item = "two" + ";" + "360 tour";
    TourFind(tour_item)
    return false;
}

function PhotoTour(){ 
    localStorage.removeItem("tourdata");  
    var tour_item = "two" + ";" + "Still photo tour";
    TourFind(tour_item)
    return false;
}

function YoutubeTour(){  
    localStorage.removeItem("tourdata"); 
    var tour_item = "two" + ";" + "Youtube tour";
    TourFind(tour_item)
    return false;
}


function TourFind(tour_kind){
    //console.log(tour_kind);
    
    var crf_token = "";
    document.cookie.split("; ").forEach(function(obj, index) {
    
        var cookie = obj.split('=');
        if(cookie[0] == 'csrftoken') {
            crf_token = cookie[1];
        }
    })

    $.ajax({
        type: "POST",
        url: "/search/",
        data: {
            'tour_kind': tour_kind
        },
        headers: {'X-CSRFToken': crf_token },
        success: function (response) {
  
            var tourdata = response.slice(0,response.length-1);         
            localStorage.setItem("tourdata", JSON.stringify(tourdata)); 
            localStorage.setItem("tour_title", JSON.stringify(tour_kind.split(';')[1]));          

           //--redirect part for 360page--//
            var direct = response[response.length-1];
            var flag = direct.split(',')[0];
            var url = direct.split(',')[1];                        
            if(flag == 'success') {
                window.open(url,"_self");
            }   
            
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            };
            console.log(msg)
        }
    });
    return false;
}

var total_tourdata = [];

function setTourInfo() {
    if (localStorage.getItem("tourdata")!=""){
        total_tourdata = JSON.parse(localStorage.getItem("tourdata"));
        title_tour = JSON.parse(localStorage.getItem("tour_title"));
        $('#explore_title').text(title_tour);
    }
    if (total_tourdata.length<4){
        for (i=1; i<=total_tourdata.length; i++){
            CurrentInfo(i); 
         }  
        page_count = 1;
    }
    else {
        for (i=1; i<=4; i++){
            CurrentInfo(i); 
         } 
        var value = total_tourdata.length/4;     
        if (value == parseInt(value)){
            page_count = parseInt(value);
        }
        else page_count = parseInt(value) + 1; 
    } 

    $("a[name=prev]").hide();
    
    if(page_count == 3){
        $("a[name=more]").hide();
    }
    else if(page_count == 2){       
        $("a[name=more]").hide();
        $("a[name=third]").hide();
    }
    else if(page_count == 1){
        $("a[name=more]").hide();
        $("a[name=second]").hide();
        $("a[name=third]").hide();
    }

    $("a[name=first]").on("click", function () {             
        var num = $("a[name=first]").text()*4;
        for (i=num-3; i<=num; i++){
            CurrentInfo(i); 
        }            
    });
    $("a[name=second]").on("click", function () { 
        var num = $("a[name=second]").text()*4;
        for (i=num-3; i<=num; i++){
            CurrentInfo(i); 
        }
    });
    $("a[name=third]").on("click", function () { 
        var num = $("a[name=third]").text()*4;
        for (i=num-3; i<=num; i++){
            CurrentInfo(i); 
        }
    });
    $("a[name=more]").on("click", function () { 
        
        var baseline = page_count - 3;
        if (current_page3<=baseline){
            current_page1+=3;
            $("a[name=first]").text(current_page1);
            current_page2+=3;
            $("a[name=second]").text(current_page2);
            current_page3+=3;
            $("a[name=third]").text(current_page3);
            $("a[name=prev]").show();
        }
        
        else if(current_page3<page_count){
            current_page3 = page_count;
            $("a[name=third]").text(current_page3);
            current_page2 = page_count - 1;
            $("a[name=second]").text(current_page2);
            current_page1 = page_count - 2;
            $("a[name=first]").text(current_page1);
            $("a[name=prev]").show();
            $("a[name=more]").hide();
        }
    });
    $("a[name=next]").on("click", function () { 
        if (current_page3<page_count){
            current_page1+=1;
            $("a[name=first]").text(current_page1);
            current_page2+=1;
            $("a[name=second]").text(current_page2);
            current_page3+=1;
            $("a[name=third]").text(current_page3);	
            $("a[name=prev]").show();
        }
        if (current_page3 == page_count){
            $("a[name=more]").hide();
        }	    
    });
    $("a[name=prev]").on("click", function () { 
        if (current_page1 > 1){
            current_page1-=1;
            $("a[name=first]").text(current_page1);
            current_page2-=1;
            $("a[name=second]").text(current_page2);
            current_page3-=1;
            $("a[name=third]").text(current_page3);	                    
        } 
        if (current_page1 == 1){
            $("a[name=prev]").hide();
        }
        $("a[name=more]").show();
                
    });
    $("a[name=last]").on("click", function () { 
        if (current_page3<page_count){
            current_page3 = page_count;
            $("a[name=third]").text(current_page3);
            current_page2 = page_count - 1;
            $("a[name=second]").text(current_page2);
            current_page1 = page_count - 2;
            $("a[name=first]").text(current_page1);
            $("a[name=prev]").show();
            $("a[name=more]").hide();
        }		        
    });       
    real_index = localStorage.getItem("newbutton");     
    DetailInfo(real_index);
    
}

$(function() {
    setTourInfo();
});

//-----------Venue Category----------------//
///--Natural--///
url_1 = '/media/filer_public_thumbnails/filer_public/84/12/8412ee16-aa93-4a2a-97c5-ef6f93c5edc1/icon_venue_natural_small.png__514x500_q85_subsampling-2.png';
///--Landmark--///
url_2 = '/media/filer_public_thumbnails/filer_public/cc/a7/cca766ea-8c99-4cb5-a6d3-a6a40a215969/icon_venue_landmark_small.png__514x500_q85_subsampling-2.png';
///--Science--///
url_3 = '/media/filer_public_thumbnails/filer_public/fa/92/fa92bdcb-7abd-40f3-b8be-9056b23b8d21/icon_venue_science_small.png__500x486_q85_subsampling-2.png';			
///--Location---///
url_4 = '/media/filer_public_thumbnails/filer_public/0a/c7/0ac78943-34b8-4cfd-894b-809ac788dbd4/icon_location.png__514x500_q85_subsampling-2.png';


//-----------Tour Type---------------------//
///--Photo--///
url_5 = '/media/filer_public_thumbnails/filer_public/b6/67/b667c187-4538-48d2-9afd-81113156ba56/icon_tour_photo-small.png__514x500_q85_subsampling-2.png';
///--360 degree--///
url_6 = '/media/filer_public_thumbnails/filer_public/ad/21/ad21166d-8ce6-4342-869a-e7902fc64232/icon_tour_360-small.png__514x500_q85_subsampling-2.png';
///--Youtube--///
url_7 = '/media/filer_public_thumbnails/filer_public/7f/e6/7fe608fb-fa67-481f-999b-29faeb1e3f35/icon_tour_youtube_small.png__514x500_q85_subsampling-2.png';
///--Other---///
url_8 = '/media/image/Icon_Unique.png';
            
var cycle_index = 0;
var detail_index1 = 0;
var detail_index2 = 0;
var detail_index3 = 0;
var detail_index4 = 0;

function CurrentInfo(number_index){       
    real_tourdata = JSON.parse(localStorage.getItem("tourdata"))[number_index-1];       
    real_tourdata = total_tourdata[number_index-1];  
    title_tour = JSON.parse(localStorage.getItem("tour_title"));

    Venue_Category = real_tourdata.Venue_Category;
    Tour_Type = real_tourdata.Tour_Type;
    Unique_Note = real_tourdata.Unique_Note;
    Experience_Title = real_tourdata.Experience_Title;
    Experience_URL = real_tourdata.Experience_URL;    
    Venue = real_tourdata.Venue;
    Venue_Address = real_tourdata.Venue_Address;
    Tour_Description = real_tourdata.Tour_Description;
    Tour_ImageLink = real_tourdata.Tour_ImageLink;
    //paramter index//
   
    cycle_index += 1;      
    if (cycle_index > 4){
        cycle_index = 1;
    } 
    if (cycle_index == 1) {
        detail_index1 = number_index;       
    }
    if (cycle_index == 2) {
        detail_index2 = number_index;        
    }
    if (cycle_index == 3) {
        detail_index3 = number_index;        
    }
    if (cycle_index == 4) {
        detail_index4 = number_index;      
    }
    //console.log(cycle_index);    
    tour_typepic = '#tour_typepic' + cycle_index;
    venue_typepic = '#venue_typepic' + cycle_index;
    location_pic = '#location_pic' + cycle_index;
    main_venuetitle = '#main_venuetitle' + cycle_index;
    tour_description = '#tour_description' + cycle_index;
    back_img = '#back_img' + cycle_index;

    //input data//
    $(main_venuetitle).text(Venue);
    $(tour_description).text(Tour_Description);    
    $(location_pic).attr('src',url_4);

    if (title_tour == '360 tour') {
        $(back_img).attr('src','/media/image/360_degree_tour.png');
    }  
    else if (title_tour == 'Still photo tour') {
        $(back_img).attr('src','/media/image/photograph_tour.png');
    } 
    else if (title_tour == 'Youtube tour') {
        $(back_img).attr('src','/media/image/youtube_tour.png');
    }  
    else if (title_tour == 'Natural History') {
        $(back_img).attr('src','/media/image/natural_history.png');
    }  
    else if (title_tour == 'Landmark') {
        $(back_img).attr('src','/media/image/landmark.jpeg');
    }  
    else if (title_tour == 'Science Museum') {
        $(back_img).attr('src','/media/image/science_museum.png');
    }   


    if (Tour_Type == '360 tour'){
        $(tour_typepic).attr('src',url_6);            
    }
    else if (Tour_Type == 'Still photo tour'){
        $(tour_typepic).attr('src',url_5);				
    }
    else if (Tour_Type == 'Youtube tour'){
        $(tour_typepic).attr('src',url_7);				
    }
    else {
        $(tour_typepic).attr('src',url_8);		
    }

    if (Venue_Category == 'Natural History'){
        $(venue_typepic).attr('src',url_1);			
    }
    else if (Venue_Category == 'Landmark'){
        $(venue_typepic).attr('src',url_2);			
    }
    else if (Venue_Category == 'Science Museum'){
        $(venue_typepic).attr('src',url_3);			
    }
    else {
        $(venue_typepic).attr('src',url_8);			
    }
}

function DetailInfo(inx){
    if (inx != 0) {
        real_tourdata = JSON.parse(localStorage.getItem("tourdata"))[inx-1];     
        Venue_Category = real_tourdata.Venue_Category;
        Tour_Type = real_tourdata.Tour_Type;
        Unique_Note = real_tourdata.Unique_Note;
        Experience_Title = real_tourdata.Experience_Title;
        Experience_URL = real_tourdata.Experience_URL;    
        Venue = real_tourdata.Venue;
        Venue_Address = real_tourdata.Venue_Address;
        Tour_Description = real_tourdata.Tour_Description;
        Tour_ImageLink = real_tourdata.Tour_ImageLink;
        ///------Expanding page-----/////
        $('#expand_Venue').text(Venue);
        $('#expand_Extitle').text(Experience_Title);
        $('#expand_Location').text(Venue_Address);
        $('#expand_Description').text(Tour_Description);
    
        if (Tour_Type == '360 tour'){
            $('#expand_icon1').attr('src',url_6);           
        }
        else if (Tour_Type == 'Still photo tour'){
            $('#expand_icon1').attr('src',url_5);				
        }
        else if (Tour_Type == 'Youtube tour'){
            $('#expand_icon1').attr('src',url_7);				
        }
        else {
            $('#expand_icon1').attr('src',url_8);		
        }

        if (Venue_Category == 'Natural History'){
            $('#expand_icon2').attr('src',url_1);			
        }
        else if (Venue_Category == 'Landmark'){
            $('#expand_icon2').attr('src',url_2);			
        }
        else if (Venue_Category == 'Science Museum'){
            $('#expand_icon2').attr('src',url_3);			
        }
        else {
            $('#expand_icon2').attr('src',url_8);			
        }
    }    
}

function DetailTour1(){    
    localStorage.setItem("newbutton", detail_index1);   
}
function DetailTour2(){    
    localStorage.setItem("newbutton", detail_index2);   
}
function DetailTour3(){    
    localStorage.setItem("newbutton", detail_index3);   
}
function DetailTour4(){    
    localStorage.setItem("newbutton", detail_index4);   
}

///---like note----///
$(document).ready(function() {
    $('.rate').click(function(){
        name = $(this).attr('src');
        if (name.search('icon_dislike.png') == -1)
            $(this).attr('src', '/media/filer_public_thumbnails/filer_public/d5/e1/d5e1a1c2-de71-429f-8878-25f398288b1b/icon_dislike.png__43x41_q85_subsampling-2.png');
        else
            $(this).attr('src', '/media/filer_public_thumbnails/filer_public/a4/47/a4479910-19fe-4332-a937-7807b3bf949b/icon_like.png__43x41_q85_subsampling-2.png');
    })			
})



	

