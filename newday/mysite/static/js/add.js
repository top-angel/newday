
function Login() {
   
    var email = $('#login-form #uname').val();
    var password = $('#login-form #psw').val();

    var crf_token = "";
    document.cookie.split("; ").forEach(function(obj, index) {
    
        var cookie = obj.split('=');
        if(cookie[0] == 'csrftoken') {
            crf_token = cookie[1];
        }
    })

    $.ajax({
        type: "POST",
        url: "/login",
        data: {
            'email': email,
            'password': password
        },
        headers: {'X-CSRFToken': crf_token },
        success: function (response) {
            var flag = response.split(',')[0];
                var url = response.split(',')[1];
                var username = response.split(',')[2];
                var email = response.split(',')[3];
                var created_date = response.split(',')[4];
                
            if(flag == 'success') {
                document.cookie = "status=login";
                window.open(url,"_self");
                localStorage.setItem("username", JSON.stringify(username)); 
                localStorage.setItem("email", JSON.stringify(email));
                localStorage.setItem("created_date", JSON.stringify(created_date));                   
            }
            
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
                $('#invalid_user').text("Please check your mail and password!")
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            };
            console.log(msg)
        }
    });
    return false;
}

function SignUp() {
    var user_name = $('#signup-form #user_name').val();
    if (user_name == ""){
        $('#signup-form #username_warning').val("Please input your name!");
    }
    else 
        $('#signup-form #username_warning').val("");

    var temp_email = $('#signup-form #newname').val();
    var temp_password1 = $('#signup-form #psw1').val();
    var temp_password2 = $('#signup-form #psw2').val();
    if (user_name != "" && temp_email!="" && temp_password1!="" && temp_password2!="" && temp_password1==temp_password2){
        $('#signup-form #warning').val("");
        var email = temp_email;
        var password = temp_password1;
        var crf_token = "";
        document.cookie.split("; ").forEach(function(obj, index) {
        
            var cookie = obj.split('=');
            if(cookie[0] == 'csrftoken') {
                crf_token = cookie[1];
            }
        })

        $.ajax({
            type: "POST",
            url: "/signup",
            data: {
                'user_name': user_name,
                'email': email,
                'password': password
            },
            headers: {'X-CSRFToken': crf_token },
            success: function (response) {               
                
                var flag = response.split(',')[0];
                var url = response.split(',')[1];
                var username = response.split(',')[2];
                var email = response.split(',')[3];
                var created_date = response.split(',')[4];
                
                if(flag == 'success') {
                    document.cookie = "status=login";
                    window.open(url,"_self");
                    localStorage.setItem("username", JSON.stringify(username)); 
                    localStorage.setItem("email", JSON.stringify(email));
                    localStorage.setItem("created_date", JSON.stringify(created_date));                   
                }
            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                };
                console.log(jqXHR)
            }
        });
    }
    else 
        $('#signup-form #warning').val("Please input correct mail or password!");
    
    return false;
}

function loginwith(info) {
    
    var crf_token = "";
    document.cookie.split("; ").forEach(function(obj, index) {
    
        var cookie = obj.split('=');
        if(cookie[0] == 'csrftoken') {
            crf_token = cookie[1];
        }
    })

    $.ajax({
        type: "POST",
        url: "/app/loginwith",
        data: info,
        headers: {'X-CSRFToken': crf_token },
        success: function (response) {
            console.log(response);
            var flag = response.split(',')[0];
            var url = response.split(',')[1];
            if(flag == 'success') {
                //document.cookie = "name="+response.split(',')[3];
                document.cookie = "status=login";
                window.open(url,"_self");
            }
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            };
            console.log(jqXHR)
        }
    });
    return false;
}

LogInWithFacebook = function() {
    FB.login(function(response) {
      if (response.authResponse) {
          var url = "https://graph.facebook.com/"+response.authResponse.userID+"?fields=name,email,picture&access_token="+response.authResponse.accessToken;
          $.ajax({
              type: "GET",
              url: url,
              success: function (response1) {
                  console.log(response1);
                  loginwith(response1);
              },
          });
        // Now you can redirect the user or do an AJAX request to
        // a PHP script that grabs the signed request from the cookie.
      } else {
        alert('User cancelled login or did not fully authorize.');
      }
    });
    return false;
  };
  window.fbAsyncInit = function() {
    FB.init({
      appId: '611197879478549',
      cookie: true, // This is important, it's not enabled by default
      version: 'v2.8'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function setProfileInfo() {
      name = document.cookie.split("; ")[0].split('=')[1];
      $('#username').html(name);
      segs = document.location.href.split('/');
      if ((segs[segs.length - 2] == "en" && segs[segs.length - 1] == "") || segs[segs.length - 2] == "signup") {
        $('#userinfo').html('');
        return;
      }
      if ((document.cookie.split("; ")[0].split('=')[0] == "name" && name != undefined) 
      || document.cookie.split("; ")[0].split('=')[1] == "login"){
        $('#userinfo').html('<li class="dropdown user user-menu" style="list-style: none">       <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">           <img id="small_profile" src="/media/filer_public_thumbnails/filer_public/81/bd/81bd5487-f59a-4109-bca0-523a26626b2f/download.jpeg__150x150_q85_subsampling-2.jpg" class="user-image" alt="User Image" style="width: 25px; height: 25px; border-radius: 50%;">           <span id="profile_username" class="hidden-xs">W Lathan</span>       </a>       <ul class="dropdown-menu">           <!-- User image -->           <li class="user-header" style="text-align: center; padding: 10px;">               <img id="medium_profile" src="/media/filer_public_thumbnails/filer_public/81/bd/81bd5487-f59a-4109-bca0-523a26626b2f/download.jpeg__150x150_q85_subsampling-2.jpg" class="img-circle" style="height: 90px; width: 90px; border: 3px solid; border-color: rgba(255,255,255,0.2); border-radius: 50%;" alt="User Image"><p>User<!-- <small>Member since Nov. 2012</small> --></p></li>           <!-- Menu Footer--><li class="user-footer"><div class="pull-left"><a href="/user-profile" class="btn btn-default btn-flat">Profile</a></div><div onclick="Signout()" class="pull-right"><a href="../" class="btn btn-default btn-flat">Sign out</a></div></li></li>')
        var username = JSON.parse(localStorage.getItem("username"));
        $('#profile_username').text(username);
        $('#username').text(username);
        var created_date = JSON.parse(localStorage.getItem("created_date"));
        $('#created_date').text(created_date);
        
      }
       
      //document.cookie = "status=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  }
  $(function() {
    setProfileInfo();  
  });

function Signout() {
    //localStorage.removeItem("username");
    //localStorage.removeItem("email");
    //localStorage.removeItem("created_date");
    localStorage.removeItem("tourdata");
    //localStorage.removeItem("imagepath");
    //localStorage.removeItem("image_set_index");
    //localStorage.removeItem("newbutton");
    //localStorage.removeItem("tour_title");
}



function  Homepage() {
   
    var furl = window.location.href;
    var len = window.location.href.length;
    
    checkflag1 = furl[len-4] + furl[len-3] + furl[len-2] + furl[len-1];
    checkflag2 = furl[len-8] + furl[len-7] + furl[len-6] + furl[len-5] + furl[len-4] + furl[len-3] + furl[len-2] + furl[len-1];
    
    if (checkflag1 != '/en/' && checkflag2 != '/signup/'){
        var home = 'home';    
        var crf_token = "";
        document.cookie.split("; ").forEach(function(obj, index) {
        
            var cookie = obj.split('=');
            if(cookie[0] == 'csrftoken') {
                crf_token = cookie[1];
            }
        })        

        $.ajax({
            type: "POST",
            url: "/main/home",
            data: {
                'home': home
                
            },
            headers: {'X-CSRFToken': crf_token },
            success: function (response) {
                var flag = response.split(',')[0];
                var url = response.split(',')[1];
                if(flag == 'success') {                     
                    window.open(url,"_self");
                }
            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                };
                console.log(msg)
            }
        });
    }
    
    return false;
}