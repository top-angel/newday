

$(document).ready(function() {
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
        var style = $("#profile-pic").attr("style");
        style = style.replace(" margin-left: -80px; margin-top: -80px;", "");
        $("#profile-pic").attr("style", style);
       //$(".file-upload").click();
    });
    var options = {
          "container": {
            "width": "100%",
            "height": 400
          },
          "viewport": {
            "width": 150,
            "height": 150,
            "margin-left": 20,
            "type": "circle",
            "border": {
              "width": 2,
              "enable": true,
              "color": "#ddd"
            }
          },
          "zoom": {
            "enable": true,
            "mouseWheel": true,
            "slider": false
          },
          "transformOrigin": "viewport"
        }
    var myphoto = $("#profile-pic").cropme(options);
    $('.cropme-container').width('100%');
    $('.cropme-container').height('400px');
    $('#save').click(function(){        
        var style = $("#profile-pic").attr("style");
        $("#profile-pic").attr("style", style + "; margin-left: -80px; margin-top: -80px;");
        myphoto.cropme('crop').then(function(output){
            $('#my-profile').attr('src', output);       
            var index_mail = JSON.parse(localStorage.getItem("email"));     
            PhotoSave(output, index_mail);
        });
    });    
});

function PhotoSave(photosource, index){
  
    

    var crf_token = "";
    document.cookie.split("; ").forEach(function(obj, index) {
    
        var cookie = obj.split('=');
        if(cookie[0] == 'csrftoken') {
            crf_token = cookie[1];
        }
    })

    $.ajax({
        type: "POST",
        url: "/photo/photosave",
        data: {
            'index': index,
            'photosource': photosource
        },
        headers: {'X-CSRFToken': crf_token },
        success: function (response) {
            var flag = response.split(',')[0];
            var url = response.split(',')[1];
            var imagepath = response.split(',')[2];
            if(flag == 'success') {
                localStorage.setItem("imagepath", JSON.stringify(imagepath));
                localStorage.setItem("image_set_index", "1"); 
                window.open(url,"_self");
            }
                      
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
                $('#invalid_user').text("Please check your mail and password!")
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            };
            console.log(msg)
        }
    });
    return false;
}


function RemoveImg(){
    real_path = '/media/filer_public_thumbnails/filer_public/81/bd/81bd5487-f59a-4109-bca0-523a26626b2f/download.jpeg__150x150_q85_subsampling-2.jpg'

    $('#my-profile').attr('src', real_path);
    $('#small_profile').attr('src', real_path);
    $('#medium_profile').attr('src', real_path);

    localStorage.removeItem("image_set_index");
    var index_mail = JSON.parse(localStorage.getItem("email"));
    var crf_token = "";
    document.cookie.split("; ").forEach(function(obj, index) {
    
        var cookie = obj.split('=');
        if(cookie[0] == 'csrftoken') {
            crf_token = cookie[1];
        }
    })

    $.ajax({
        type: "POST",
        url: "/photo/photoremove",
        data: {
            'index_mail': index_mail            
        },
        headers: {'X-CSRFToken': crf_token },
        success: function (response) {
            var flag = response.split(',')[0];
            var url = response.split(',')[1];
            
            if(flag == 'success') {
                
                localStorage.setItem("image_set_index", "0"); 
                window.open(url,"_self");

            }
                      
        },
        error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
                $('#invalid_user').text("Please check your mail and password!")
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            };
            console.log(msg)
        }
    });
    return false;
}

$(function() {
    
    var index = JSON.parse(localStorage.getItem("image_set_index"));
    console.log(index);
    
    if (index != null){
        var path = JSON.parse(localStorage.getItem("imagepath"));
        real_path = '/media/' + path;
        
    }
    else {
        real_path = '/media/filer_public_thumbnails/filer_public/81/bd/81bd5487-f59a-4109-bca0-523a26626b2f/download.jpeg__150x150_q85_subsampling-2.jpg'
    }
    $('#my-profile').attr('src', real_path);
    $('#small_profile').attr('src', real_path);
    $('#medium_profile').attr('src', real_path);
 
    
    console.log(real_path);
});