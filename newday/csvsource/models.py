from django.db import models
from django.utils import timezone

# Create your models here.

class Newdaydatabase(models.Model):
  
    Venue_Category = models.CharField(max_length=300)
    Tour_Type = models.CharField(max_length=300)
    Unique_Note = models.CharField(max_length=300)

    Experience_Title = models.CharField(max_length=300)
    Experience_URL = models.TextField()
    Venue = models.CharField(max_length=300)
    Venue_Address = models.CharField(max_length=300)

    Tour_Description = models.TextField()
    Tour_ImageLink = models.TextField()
   
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.Tour_Type
