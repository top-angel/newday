from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import HttpResponse, HttpResponseRedirect

from csvsource.models import Newdaydatabase

import json


@api_view(['POST'])
def csvupload(request):
    data = request.POST.get('csvdata')    
    readLine = data.split('\n')
    
    for line in readLine:
        column = line.split(',')
        new_column = []
        res = [i for i in column if '"' in i]
        col_idx = res_idx = 0
        while (col_idx < len(column)):
            if (column[col_idx].find('"') != -1):
                idx = column.index(res[res_idx + 1])
                new_str = ''
                for x in range(col_idx, idx + 1):
                    if (x == col_idx):
                        new_str += column[x]
                    else:
                        new_str += ',' + column[x]
                    column[x] = ''
                new_str = new_str.replace('"', '')
                new_column.append(new_str)
                col_idx = idx + 1
                res_idx = res_idx + 2
            else:
                new_column.append(column[col_idx])
                column[col_idx] = ''
                col_idx = col_idx + 1
        column = new_column
        Newdaydatabase.objects.create(Venue_Category = column[0], Tour_Type = str(column[1]),Unique_Note=str(column[2]), Experience_Title=str(column[3]), Experience_URL=str(column[4]), Venue=str(column[5]), Venue_Address=str(column[6]),Tour_Description=str(column[7]), Tour_ImageLink=str(column[8]))
    
    if (data != ''):
        result = 'success'
    return HttpResponse(result)


@api_view(['POST'])
def toursearch(request):
    tour_kind = request.POST.get('tour_kind')
    selection = tour_kind.split(';')[0]
    tour_index = tour_kind.split(';')[1]
    if (selection == 'one'):
        tourdata = Newdaydatabase.objects.filter(Venue_Category=tour_index)    

    if (selection == 'two'):
        tourdata = Newdaydatabase.objects.filter(Tour_Type=tour_index)

    # tourdata = Newdaydatabase.objects.filter(Tour_Type=tour_kind)
   
    response = []
    for tourdata in tourdata:
        tmpvar = {}
        tmpvar = { 'Venue_Category':tourdata.Venue_Category, 'Tour_Type':tourdata.Tour_Type, 'Unique_Note':tourdata.Unique_Note, 'Experience_Title':tourdata.Experience_Title, 'Experience_URL':tourdata.Experience_URL,'Venue':tourdata.Venue,'Venue_Address':tourdata.Venue_Address,'Tour_Description':tourdata.Tour_Description,'Tour_ImageLink':tourdata.Tour_ImageLink, }
        response.append(tmpvar)
    
    result = 'success'+','+"http://" + request.get_host()+"/360tours"
    response.append(result)     
    return HttpResponse(json.dumps(response), content_type="application/json")
   


   