from django.urls import path
from . import views

urlpatterns = [
    path('upload/', views.csvupload, name='csvuploads'),
    path('search/', views.toursearch, name='searches'),
]