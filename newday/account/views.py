from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.shortcuts import render
from rest_framework import generics, status

##########
from django.shortcuts import render, reverse, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
import json
from account.models import Account, Profile
from passlib.hash import pbkdf2_sha256

import uuid
import base64
from django.core.files.base import ContentFile
import os

@api_view(['POST'])
def loginfun(request):
    email = request.POST.get('email')
    password = request.POST.get('password')
    userdata = Account.objects.filter(email=email)

    # currentdata = Profile.objects.filter(email=email)
    # path = currentdata[0].photo.name

    # user = authenticate(username=username, password=password)  
    # if user:
    if pbkdf2_sha256.verify(password, userdata[0].password):
        # login(request, user)
        # result = 'success'+','+"http://" + request.get_host()+"/landingpage"
        # return HttpResponse(result)
        created = userdata[0].date_joined
        date_string = created.strftime('%Y-%m-%d')

        result = 'success'+','+"http://" + request.get_host() + "/landingpage" + ',' + userdata[0].username + ',' + userdata[0].email + ',' + date_string 
        return HttpResponse(result)

    else:               
        # ddd = messages.error(request, 'username or password not correct')       
        return redirect('')

@api_view(['POST'])
def signupfun(request):
    user_name = request.POST.get('user_name')
    email = request.POST.get('email')
    password = request.POST.get('password')
    
    Account.objects.create_user(email, user_name, password)
    userdata = Account.objects.filter(email=email)

    created = userdata[0].date_joined
    date_string = created.strftime('%Y-%m-%d')       

    result = 'success'+','+"http://" + request.get_host() + "/landingpage" + ',' + user_name + ',' + userdata[0].email + ',' + date_string
    return HttpResponse(result)

@api_view(['POST'])
def loginwith(request):
    email = request.POST.get('email')
    name = request.POST.get('name')

    if Account.objects.filter(email=email):
        result = 'success'+','+"http://" + request.get_host()+"/landingpage"
    else:
        res = Account.objects.create_fbuser(email, name)
        result = 'success'+','+"http://" + request.get_host()+"/user-profile"+','+email+','+name
    return HttpResponse(result)

@api_view(['POST'])
def home(request):
    home = request.POST.get('home')   
    if (home == 'home'):
        result = 'success'+','+"http://" + request.get_host()+"/landingpage"    
    return HttpResponse(result)

@api_view(['POST'])
def photosave(request):
    email = request.POST.get('index') 
    name_type = email + '.'
    image_b64 = request.POST.get('photosource') # This is your base64 string image
    format, imgstr = image_b64.split(';base64,')
    ext = format.split('/')[-1]
    data = ContentFile(base64.b64decode(imgstr), name=name_type + ext)
    Profile.objects.create(email=email, photo=data) 

    currentdata = Profile.objects.filter(email=email)
    path = currentdata[0].photo.name

    result = 'success' + ',' + "http://" + request.get_host()+"/user-profile" + ',' + path  
    # result = 'success' + ',' + path    
    return HttpResponse(result)

@api_view(['POST'])
def photoremove(request):
    email = request.POST.get('index_mail')     
    currentdata = Profile.objects.filter(email=email)
    if email == currentdata[0].email and currentdata[0].photo != "":         
        path = currentdata[0].photo            
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
        os.remove(MEDIA_ROOT + path.name)  
        currentdata.delete() 
    result = 'success' + ',' + "http://" + request.get_host()+"/user-profile"    
    return HttpResponse(result)


