from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from passlib.hash import pbkdf2_sha256
from django.core.files import File
import os
# Create your models here.

class UserManager(BaseUserManager):

    def create_user(self, email, name, password = '', socialid = '', is_active=False, is_staff=False, is_admin=False):
        if not email: 
            raise ValueError("Users must have an email address")
        email = self.normalize_email(email)
    

        user_obj = self.model(  email=email,
                                username = name,
                                password=pbkdf2_sha256.encrypt(password, rounds=150000),
                                socialid = socialid,
                                is_staff = is_staff,
                                is_admin = is_admin,
                                is_active = is_active
                            )
        
        # user_obj.set_password(password)
        
        user_obj.save(using=self._db)
        return user_obj

    def create_fbuser(self, email, name, socialid = '', is_active=False, is_staff=False, is_admin=False):
        if not email: 
            raise ValueError("Users must have an email address")
        email = self.normalize_email(email)
    

        user_obj = self.model(email=email,
                                username = name,
                                is_staff = is_staff,
                                is_admin = is_admin,
                                is_active = is_active
                            )
        
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email, password=None):
        user = self.create_user(
            email,
            password = password,
            is_staff = True
        )
        return user

    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password = password,
            is_staff = True,
            is_admin = True
        )
        return user  

    # def get_file(data):        
    #     return File(data)
    
    

class Account(AbstractBaseUser):
    email                   = models.EmailField(verbose_name="email", max_length=30, unique=True)
    socialid                = models.CharField(max_length=100, null=True)
    username                = models.CharField(max_length=100, null=True)
    password                = models.CharField(max_length=100, null=True)
    date_joined             = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login              = models.DateTimeField(verbose_name="last login", auto_now_add=True)
    is_admin                = models.BooleanField(default=False)
    is_active               = models.BooleanField(default=False)
    is_staff                = models.BooleanField(default=False)
    is_superuser            = models.BooleanField(default=False)
    full_name               = models.CharField(max_length=100)
   
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

class Profile(models.Model):
    email                   = models.EmailField(verbose_name="email", max_length=30, unique=True)
    photo = models.ImageField(upload_to="image", null=True)

    



  


