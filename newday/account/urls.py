from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login', views.loginfun, name='logins'),
    url(r'^app/loginwith', views.loginwith, name='loginwiths'),
    url(r'^signup', views.signupfun, name='signups'),
    url(r'^main/home', views.home, name='homes'),
    url(r'^photo/photosave', views.photosave, name='photosaves'),
    url(r'^photo/photoremove', views.photoremove, name='photoremoves'),
]